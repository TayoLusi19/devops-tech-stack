# Docker
## What It Does: 
Docker is a platform for developing, shipping, and running applications in containers. Containers allow you to package up an application with all of its dependencies into a standardized unit for software development.

## How to Use It:
Install Docker on your machine from the [official Docker website.](https://docs.docker.com/engine/install/)
To run an application in a Docker container, you need to create a Dockerfile, which specifies the environment and commands to run the app.

**Syntax Example:**

```bash
# Use an official Python runtime as a parent image
FROM python:3.7-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . .

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Run app.py when the container launches
CMD ["python", "app.py"]
```

# Terraform

## What It Does: 
Terraform is an infrastructure as code (IaC) tool that allows you to build, change, and version control infrastructure safely and efficiently. It can manage service providers as well as custom in-house solutions. Download Terraform [here](https://developer.hashicorp.com/terraform/install)

## How to Use It:
Install Terraform following the instructions on the HashiCorp website.
Create a Terraform configuration file (main.tf) that defines your infrastructure.

**Syntax Example:**

```bash
provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "example" {
  ami           = "ami-0c55b159cbfafe1f0"
  instance_type = "t2.micro"
}
```

# Ansible

## What It Does: 
Ansible is an open-source automation tool for software provisioning, configuration management, and application deployment. Ansible uses playbooks to describe automation jobs, and Inventory files to define the servers to deploy these jobs to.

##How to Use It:
Install Ansible on your machine. Instructions can be found on the official Ansible documentation.
Create a playbook (YAML file) to define tasks to be performed on the target servers.

**Syntax Example:**

```bash
- name: Update web servers
  hosts: webservers
  become: yes
  tasks:
    - name: Ensure nginx is at the latest version
      apt:
        name: nginx
        state: latest
```

### Configuring Control Machine To Target Machine

1. create hosts file
```bash
vim hosts
```

2. Inside the host file configure your target machine ip and private key
```bash
[ec2]
ec2-13-59-140-45.us-east-2.compute.amazonaws.com ansible_user=ec2-user ansible_ssh_private_key_file=/Users/tayo/Downloads/ansible.pem
ec2-3-16-41-83.us-east-2.compute.amazonaws.com ansible_user=ec2-user ansible_ssh_private_key_file=/Users/tayo/Downloads/ansible.pem
```
- Change the ip address and the ansible_user and the location to your private key

3. Test the connection using the command below
```bash
ansible ec2 -i hosts -m ping
```


# Jenkins
## What It Does: 
Jenkins is an open-source automation server used to automate the parts of software development related to building, testing, and deploying, facilitating continuous integration and continuous delivery.

## How to Use It:
Install Jenkins by following the official Jenkins installation guide.
Configure a job in Jenkins to pull from your source control, build the project, run tests, and deploy.

**Example of a Jenkinsfile (Pipeline Syntax):**

```bash
pipeline {
    agent any 
    stages {
        stage('Build') { 
            steps {
                // Commands to build your project
                sh 'echo "Building..."'
            }
        }
        stage('Test') { 
            steps {
                // Commands to test your project
                sh 'echo "Testing..."'
            }
        }
        stage('Deploy') { 
            steps {
                // Commands to deploy your project
                sh 'echo "Deploying..."'
            }
        }
    }
}
```





